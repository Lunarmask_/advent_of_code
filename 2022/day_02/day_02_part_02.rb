require 'csv'

#
#  Advent of code day 2
#
#  [a: "rock", b: "paper", c: "scissors"]
#  [x: "rock", y: "paper", z: "scissors"]
#
#  [rock: 1, paper: 2, scissors: 3]
#  [lose: 0, draw: 3, win: 6]
#

points = 0
hand = {"Z" => {"A" => 2, "B" => 3, "C" => 1}, "Y" => {"A" =>1, "B" => 2, "C" => 3}, "X" => {"A" => 3, "B" => 1, "C" => 2}}

CSV.foreach("input.csv", col_sep: " ") do |csv|
  them = csv[0]
  us = csv[1]

  points += hand[us][them]

  points += 3 if us == "Y"
  points += 6 if us == "Z"
end

puts points
