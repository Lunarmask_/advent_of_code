require 'csv'

#
#  Advent of code day 2
#
#  [a: "rock", b: "paper", c: "scissors"]
#  [x: "rock", y: "paper", z: "scissors"]
#
#  [rock: 1, paper: 2, scissors: 3]
#  [lose: 0, draw: 3, win: 6]
#

points = 0
assignment = {"A" => 1, "B" => 2, "C" => 3, "X" => 1, "Y" => 2, "Z" => 3}

CSV.foreach("input.csv", col_sep: " ") do |csv|
  them = assignment[csv[0]]
  us = assignment[csv[1]]

  points += us

  case
  when us == 1 && them == 3
    points += 6
  when us == 2 && them == 1
    points += 6
  when us == 3 && them == 2
    points += 6
  when us == them
    points += 3
  end
end

puts points
