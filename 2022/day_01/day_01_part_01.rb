#!ruby
require 'csv'

# read_file
# calculate
# store value
# calculate (replace or keep previous valueH)

highest = 0
calories = 0

CSV.foreach("input.csv", skip_blanks: false) do |line|

  if line.empty?
    highest = calories if calories > highest
    calories = 0
  else
    calories += line.first.to_i
  end
end

puts highest
