#!ruby
require 'csv'

# read_file
# calculate
# store value
# calculate (replace or keep previous valueH)

first = 0
second = 0
third = 0

calories = 0

CSV.foreach("input.csv", skip_blanks: false) do |line|

  if line.empty?
    case
    when calories > first
      third  = second
      second = first
      first  = calories
    when calories > second
      third  = second
      second = calories
    when calories > third
      third  = calories
    end
    calories = 0
  else
    calories += line.first.to_i
  end
end

puts " first: #{first}"
puts "second: #{second}"
puts " third: #{third}"
puts " total: #{first+second+third}"
