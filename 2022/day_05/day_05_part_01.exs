defmodule Day05Part01 do


#          [L][M]      [M]
#       [D][R][Z]      [C][L]
#       [C][S][T][G]   [V][M]
# [R]   [L][Q][B][B]   [D][F]
# [H][B][G][D][Q][Z]   [T][J]
# [M][J][H][M][P][S][V][L][N]
# [P][C][N][T][S][F][R][G][Q]
# [Z][P][S][F][F][T][N][P][W]
#  1  2  3  4  5  6  7  8  9

  def run() do
    starting_state = [
      ["R", "H", "M", "P", "Z"],
      ["B", "J", "C", "P"],
      ["D", "C", "L", "G", "H", "N", "S"],
      ["L", "R", "S", "Q", "D", "M", "T", "F"],
      ["M", "Z", "T", "B", "Q", "P", "S", "F"],
      ["G", "B", "Z", "S", "F", "T"],
      ["V", "R", "N"],
      ["M", "C", "V", "D", "T", "L", "G", "P"],
      ["L", "M", "F", "J", "N", "Q", "W"]
    ] |> IO.inspect(label: "starting")

    File.read!("input.csv")
    |> String.split("\n")
    |> Enum.map(&String.trim_trailing/1)
    |> Enum.reject(&(line_empty?(&1)))
    |> Enum.reduce(starting_state, fn line, state ->

      [amount,from,to] = String.split(line, ",") |> Enum.map(&String.to_integer/1)

      new_from = from - 1
      new_to = to - 1

      from_state = Enum.at(state, new_from)
      to_state   = Enum.at(state, new_to)
      taken      = Enum.take(from_state, amount)

      state
      |> apply_from_change(taken, new_from, from_state)
      |> apply_to_change(taken, new_to, to_state)
    end)
    |> IO.inspect(label: "FINAL")
  end

  def apply_from_change(state, taken, from, from_state) do
    List.replace_at(state, from, (from_state -- taken))
  end

  def apply_to_change(state, taken, to, to_state) do
    List.replace_at(state, to, (Enum.reverse(taken) ++ to_state))
  end

  def line_empty?(""), do: true
  def line_empty?(_), do: false
end

Day05Part01.run()
