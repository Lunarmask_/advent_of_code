defmodule Day04Part01 do

  def run() do
    split_to_int = fn str -> String.split(str, "-") |> Enum.map(&String.to_integer/1) end

    File.read!("input.csv")
    |> String.split("\n")
    |> Enum.map(&String.trim_trailing/1)
    |> Enum.reject(&(line_empty?(&1)))
    |> Enum.reduce([], fn line, acc ->

      [r1, r2] = String.split(line, ",")

      [r1a, r1b] = split_to_int.(r1)
      [r2x, r2y] = split_to_int.(r2)

      r1_big = (r1a <= r2x) && (r1b >= r2y)
      r2_big = (r2x <= r1a) && (r2y >= r1b)

      r2x_small = Enum.to_list(r1a..r1b) |> Enum.member?(r2x)
      r2y_small = Enum.to_list(r1a..r1b) |> Enum.member?(r2y)

      r1a_small = Enum.to_list(r2x..r2y) |> Enum.member?(r1a)
      r1b_small = Enum.to_list(r2x..r2y) |> Enum.member?(r1b)

      if r1_big || r2_big || r1a_small || r1b_small || r2x_small || r2y_small do
        acc ++ [(r1_big || r2_big || r1a_small || r1b_small || r2x_small || r2y_small)]
      else
        acc
      end
    end)
    |> length()
    |> IO.inspect(label: "Length")

  end

  def line_empty?(""), do: true
  def line_empty?(_), do: false
end

Day04Part01.run()
