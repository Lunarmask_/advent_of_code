defmodule Day03Part01 do

  @doc """
  streaming() attempts to perform as much work as possible through a filestream.

  Even though we need to reduce, we can't use Enum.reduce() because it is EAGER and not compatible with lazy-Stream.
  Using a Enum.reduce would need to wait for the stream to finish before evaluating the enumerable.

  In order to get around this, we will use an Agent to maintain state, and then send messages to it via Stream.each()

  This will be slower for smaller files and hopefully faster and less memory for larger files.
  """
  def streaming() do
    points_list = make_points_list()
    {:ok, agent} = Agent.start_link(fn -> 0 end)

    File.stream!("input.csv")
    |> Stream.map(&String.trim_trailing/1)
    |> Stream.reject(&(line_empty?(&1)))
    |> Stream.each(fn line ->
      length = String.length(line)

      char = line
             |> String.to_charlist
             |> Enum.split(Kernel.div(length, 2))
             |> find_character()
             |> String.to_atom

      Agent.update(agent, fn state -> state + points_list[char] end)
    end)
    |> Stream.run() # evaluates the stream

    IO.inspect(Agent.get(agent, &(&1)), label: "FINAL POINTS")
    Agent.stop(agent)
  end

  @doc """
  reading() just reads the whole file into memory to perform enum actions against it
  """
  def reading() do
    points_list = make_points_list()

    final_points = File.read!("input.csv")
    |> String.split("\n")
    |> Enum.map(&String.trim_trailing/1)
    |> Enum.reject(&(line_empty?(&1)))
    |> Enum.reduce(0, fn line, acc ->
      length = String.length(line)

      character = line
                   |> String.to_charlist
                   |> Enum.split(Kernel.div(length, 2))
                   |> find_character()
                   |> String.to_atom

      acc + points_list[character]
    end)

    IO.inspect(final_points, label: "FINAL POINTS")
  end

  def find_character({one, two}) do
    Enum.reduce_while(one, 0, fn char, _acc ->
      Enum.member?(two, char) && {:halt, List.to_string([char])} || {:cont, 0}
    end)
  end

  @doc """
  There is no alphabetical enumerator for elixir from what I could find.
  This is a an attempt to programatically build a linked-list assignment table
  """
  def make_points_list() do
    lowercase = Enum.map(?a..?z, fn(n) -> <<n>> end) |> Enum.map(&String.to_atom/1) |> Enum.with_index(1)
    uppercase = Enum.map(?A..?Z, fn(n) -> <<n>> end) |> Enum.map(&String.to_atom/1) |> Enum.with_index(27)
    (lowercase ++ uppercase)
  end

  def line_empty?(""), do: true
  def line_empty?(_), do: false
end

Day03Part01.streaming()

